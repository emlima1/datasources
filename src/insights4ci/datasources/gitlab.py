#!/usr/bin/env python3
import logging
from datetime import datetime, timedelta
from functools import cached_property

import gitlab
from insights4ci.datasources.common import (
    GenericDataSource,
    GenericJob,
    GenericPipeline,
    GenericRunner,
    GenericTestResult,
)

LOG = logging.getLogger("insights4ci.datasources.gitlab")

GITLAB_ENDPOINT = "https://gitlab.com"
GITLAB = gitlab.Gitlab(GITLAB_ENDPOINT)

DATE_FORMAT = "%Y-%m-%dT%H:%M:%S"


class DataSource(GenericDataSource):
    @staticmethod
    def fetch_pipelines_since(
        namespace, date_from=datetime.now() - timedelta(days=30)
    ):
        LOG.info(f"Fetching pipelines for project {namespace}...")
        project = GITLAB.projects.get(namespace)
        result = []
        pipeline_generator = project.pipelines.list(iterator=True)
        for pipeline in pipeline_generator:
            pipeline_date = datetime.strptime(
                pipeline.attributes.get("created_at").split(".")[0],
                DATE_FORMAT,
            )
            if pipeline_date <= date_from:
                break
            status = pipeline.attributes.get("status")
            if status in ["running", "skipped", "canceled"]:
                continue

            # For now, let's skip empty pipelines (without test reports).
            suites = pipeline.test_report.get().test_suites
            if not suites:
                continue
            result.append(Pipeline(pipeline.attributes, project))
        return result


class Pipeline(GenericPipeline):
    def __init__(self, data, project):
        super().__init__(data)
        self.project = project

    @cached_property
    def _raw_pipeline(self):
        return self.project.pipelines.get(self.external_id)

    @property
    def external_id(self):
        return self.data.get("id")

    @property
    def status(self):
        return self.data.get("status")

    @property
    def sha(self):
        return self.data.get("sha")

    @property
    def ref(self):
        return self.data.get("ref")

    @property
    def created_at(self):
        return self.data.get("created_at")

    @property
    def started_at(self):
        return self.data.get("started_at")

    @property
    def ended_at(self):
        return self.data.get("finished_at")

    @property
    def external_url(self):
        return self.data.get("web_url")

    @property
    def jobs(self):
        LOG.info(f"Fetching jobs for pipeline {self.external_id}...")
        if not self._jobs:
            self._jobs = [
                Job(
                    job.attributes,
                    Runner(job.runner) if job.runner else None,
                    self,
                )
                for job in self._raw_pipeline.jobs.list(
                    per_page=100, as_list=False
                )
            ]
        return self._jobs

    @cached_property
    def report(self):
        return self._raw_pipeline.test_report.get()


class Job(GenericJob):
    @property
    def external_id(self):
        return self.data.get("id")

    @property
    def name(self):
        return self.data.get("name")

    @property
    def status(self):
        return self.data.get("status")

    @property
    def stage(self):
        return self.data.get("stage")

    @property
    def external_url(self):
        return self.data.get("web_url")

    @property
    def started_at(self):
        return self.data.get("started_at")

    @property
    def created_at(self):
        return self.data.get("created_at")

    @property
    def ended_at(self):
        return self.data.get("finished_at")

    @cached_property
    def test_results(self):
        LOG.info(f"Fetching test runs for job {self.external_id}...")
        result = []
        for suite in self.pipeline.report.test_suites:
            if suite["name"] == self.name:
                result = [
                    TestResult(test_case, self)
                    for test_case in suite["test_cases"]
                ]
                break
        return result


class Runner(GenericRunner):
    @property
    def external_id(self):
        return self.data.get("id")

    @property
    def name(self):
        return self.data.get("name")

    @property
    def owner(self):
        return "gitlab"

    @property
    def architecture(self):
        # TODO: Fix this for custom runners
        return "x86_64"

    @property
    def platform(self):
        # TODO: Fix this for custom runners
        return "Linux"

    @property
    def tags(self):
        self.data.get("tag_list")


# class Test:
# So far, there is no need to create a Test object here.


class TestResult(GenericTestResult):
    @property
    def name(self):
        return self.data.get("name")

    @property
    def class_name(self):
        return self.data.get("classname")

    @property
    def status(self):
        return self.data.get("status")

    @property
    def execution_time(self):
        return self.data.get("execution_time")
