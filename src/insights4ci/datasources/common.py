import json
from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from uuid import uuid4


class GenericObj:
    """Generic remote object when representing back end data."""

    def __init__(self, data):
        self.data = data

    def __hash__(self):
        return hash(self.as_json())

    def as_dict(self):
        pass

    def as_json(self, sort_keys=True, indent=4):
        return json.dumps(self.as_dict(), sort_keys=sort_keys, indent=indent)


class GenericDataSource(ABC):
    @staticmethod
    @abstractmethod
    def fetch_pipelines_since(
        namespace, date_from=datetime.now() - timedelta(days=30)
    ):
        """
        It fetches pipelines since specified date to the latest entry.

        :param namespace: namespace of datasource
        :type namespace: str
        :param date_from: the date from when the data will be fetched
        :type date_from: datetime
        :returns: Pipelines fetched from datasources.
        :rtype: list of GenericPipeline
        """
        pass


class GenericPipeline(GenericObj):
    def __init__(self, data):
        super().__init__(data)
        self._jobs = None

    @property
    @abstractmethod
    def external_id(self):
        pass

    @property
    @abstractmethod
    def status(self):
        pass

    @property
    @abstractmethod
    def sha(self):
        pass

    @property
    @abstractmethod
    def ref(self):
        pass

    @property
    @abstractmethod
    def created_at(self):
        pass

    @property
    @abstractmethod
    def started_at(self):
        pass

    @property
    @abstractmethod
    def ended_at(self):
        pass

    @property
    @abstractmethod
    def external_url(self):
        pass

    @property
    @abstractmethod
    def jobs(self):
        """List of Jobs objects inside a pipeline.

        Each backend should implement this property.

        :returns: Jobs inside pipeline.
        :rtype: list of GenericJob
        """
        pass

    def as_dict(self):
        return {
            "external_id": self.external_id,
            "status": self.status,
            "sha": self.sha,
            "ref": self.ref,
            "created_at": self.created_at,
            "started_at": self.started_at,
            "ended_at": self.ended_at,
            "external_url": self.external_url,
            "jobs": [jb.as_dict() for jb in self.jobs],
        }


class GenericJob(GenericObj):
    def __init__(self, data, runner=None, pipeline=None):
        super().__init__(data)
        self.runner = runner
        self.pipeline = pipeline

    @property
    @abstractmethod
    def external_id(self):
        pass

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def status(self):
        pass

    @property
    @abstractmethod
    def stage(self):
        pass

    @property
    @abstractmethod
    def external_url(self):
        pass

    @property
    @abstractmethod
    def created_at(self):
        pass

    @property
    @abstractmethod
    def started_at(self):
        pass

    @property
    @abstractmethod
    def ended_at(self):
        pass

    @property
    @abstractmethod
    def test_results(self):
        """List of TestResult objects inside a Job.

        Each backend should implement this property.

        :returns: Tests results inside job.
        :rtype: list of GenericTestResult
        """
        pass

    def as_dict(self):
        return {
            "external_id": self.external_id,
            "name": self.name,
            "status": self.status,
            "stage": self.stage,
            "external_url": self.external_url,
            "created_at": self.created_at,
            "started_at": self.started_at,
            "ended_at": self.ended_at,
            "runner": self.runner.as_dict() if self.runner else None,
            "test_results": [tr.as_dict() for tr in self.test_results],
        }


class GenericRunner(GenericObj):
    @property
    @abstractmethod
    def external_id(self):
        pass

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def owner(self):
        pass

    @property
    @abstractmethod
    def architecture(self):
        pass

    @property
    @abstractmethod
    def platform(self):
        pass

    @property
    @abstractmethod
    def tags(self):
        pass

    def as_dict(self):
        return {
            "external_id": self.external_id,
            "name": self.name,
            "owner": self.owner,
            "architecture": self.architecture,
            "platform": self.platform,
            "tags": self.tags,
        }


class GenericTestResult(GenericObj):
    def __init__(self, data, job=None):
        super().__init__(data)
        self.job = job
        self.__id = str(uuid4())

    @property
    def external_id(self):
        return self.__id

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def class_name(self):
        pass

    @property
    @abstractmethod
    def status(self):
        pass

    @property
    @abstractmethod
    def created_at(self):
        pass

    @property
    @abstractmethod
    def started_at(self):
        pass

    @property
    @abstractmethod
    def execution_time(self):
        pass

    def as_dict(self):
        return {
            "external_id": self.external_id,
            "name": self.name,
            "class_name": self.class_name,
            "status": self.status,
            "created_at": self.created_at,
            "started_at": self.started_at,
            "execution_time": self.execution_time,
        }
